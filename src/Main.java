import task2.*;
import task2.entry.Entry;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
      Main m = new Main();
        String s = m.findTelephone();
        System.out.println(s);


    }

    public List<String> findTelephonesWithLoop() {
        List<String> list = new ArrayList<>();
        for (Entry e : new PhoneBookService().createAphoneBook()) {
            list.add(e.getPerson().getHomeAddress().getTelephone());
        }
        return list;
    }

    /**
     * select homeAddress from Entries
     * join Person
     * join Address
     */
    public String findTelephone() {
        PhoneBookService pbs = new PhoneBookService();
        List<Entry> entries = pbs.createAphoneBook();
        return entries.get(0).getPerson().getHomeAddress().getTelephone();
    }


    /**
     * select homeAddress from Entries
     * join Person
     * join Address
     */
    public List<String> findTelephones() {
        PhoneBookService pbs = new PhoneBookService();
        List<Entry> entries = pbs.createAphoneBook();
        return entries.stream()
                .map(e -> e.getPerson())
                .map(p -> p.getHomeAddress())
                .map(a -> a.getTelephone())
                .collect(Collectors.toList());
    }

    /**
     * Bitte zeig eine Liste mit den IDs von den Objekten Entry.
     * select id from entries
     */
    public List<Integer> findIds() {
        PhoneBookService pbs = new PhoneBookService();
        List<Entry> entries = pbs.createAphoneBook();
        return entries.stream()
                .map(e -> e.getId())  // select
                .collect(Collectors.toList());

    }

    /**
     * Bitte zeig eine Liste mit den IDs von den Objekten Entry.
     * select id from entries
     */
    public List<Integer> findIdsWhereIdIsGreaterThan2() {
        PhoneBookService pbs = new PhoneBookService();
        List<Entry> entries = pbs.createAphoneBook();
        return entries.stream()
                //.filter(e -> e.getId() > 2) //same as filter line
                .map(e -> e.getId())  // select
                .filter(e -> e > 2)
                .collect(Collectors.toList());

    }
}