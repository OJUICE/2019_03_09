package task3;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

public class LoopingExamples {

    private  JLabelFactory jlabelFactory = new JLabelFactory();
    List<Integer> integers = List.of(1, 7, 9, 5, 2, 33, 104, 58, 14, 10);

    public static void main(String[] s) {
        new LoopingExamples().loopExample3();
        new LoopingExamples().createJframe();

    }

    private  void createJframe() {
        JFrame frame = new JFrame("Lukasz");

        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());

        for (int i = 0; i < 5; i++) {
            jlabelFactory.createJlabel(frame, panel);
        }
        jlabelFactory.createJlabel02(frame, panel);
        frame.setSize(800, 700);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }


    private void loopExample1() {
        for (Integer e : integers) {
            System.out.println(e);
        }
    }

    public void loopExample2() {
        List<Integer> sorted = integers.stream().sorted().collect(Collectors.toList());
        for (Integer e : sorted) {
            System.out.println(e);
        }
    }

    public void loopExample3() {
        System.out.println("hier sind die zahlen: ");
        for (int i = 5; i < integers.size(); i++) {
            Integer e = integers.get(i);
            System.out.println(i + ". " + e);
            System.out.println(i + e);
            System.out.println(i + " " + e);
        }
    }

    public void loopExample4() {
        int i = 0;
        while (i < integers.size()) {
            Integer e = integers.get(i);
            System.out.println(e);
            i++;
        }
    }
}
