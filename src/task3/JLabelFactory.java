package task3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class JLabelFactory {

    public void createJlabel(JFrame frame, JPanel panel) {
        JLabel label = new JLabel("Oliver!");

        JButton button = new JButton();
        button.setText("Press me");

        panel.add(label);
        panel.add(button);

        frame.add(panel);
    }

    public void createJlabel02(JFrame frame, JPanel panel) {
        JLabel label = new JLabel("Arsch!");

        JButton button = new JButton();
        button.setText("Drueck mich bitte");

        panel.add(label);
        panel.add(button);

        frame.add(panel);
    }

    private static class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

        }
    }
}
