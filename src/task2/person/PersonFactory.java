package task2.person;

import task2.address.AddressFactory;

import java.time.LocalDate;
import java.time.Month;

public class PersonFactory {
    
    private AddressFactory addressFactory = new AddressFactory();

    public Person createPerson01() {
        Person p01= new Person();
        p01.setFirstName("Hans");
        p01.setLastName("Wurst");
        p01.setDob(LocalDate.of(1966, Month.FEBRUARY,22));
        p01.setHomeAddress(addressFactory.createHomeAddress());
        p01.setSex(Sex.MALE);
        p01.setWorkAddress(addressFactory.createWorkAddress());
        return p01;
    }

    public Person createPerson2() {
        Person p02= new Person();
        p02.setFirstName("Karl");
        p02.setLastName("Heinz");
        p02.setDob(LocalDate.of(1990, Month.MAY,15));
        p02.setHomeAddress(addressFactory.createHomeAddress2());
        p02.setSex(Sex.MALE);
        p02.setWorkAddress(addressFactory.createWorkAddress());
        return p02;
    }

    public Person createPerson3() {
        Person p02= new Person();
        p02.setFirstName("Horst");
        p02.setLastName("Mayer");
        p02.setDob(LocalDate.of(1942, Month.MAY,25));
        p02.setHomeAddress(addressFactory.createHomeAddress3());
        p02.setSex(Sex.MALE);
        p02.setWorkAddress(addressFactory.createWorkAddress3());
        return p02;
    }

    public Person createPerson04() {
        Person p04 = new Person();
        p04.setFirstName("Bubba");
        p04.setLastName("Gump");
        p04.setDob(LocalDate.of(1969, Month.JULY, 12));
        p04.setSex(Sex.MALE);
        p04.setHomeAddress(addressFactory.createHomeAddress4());
        p04.setWorkAddress(addressFactory.createWorkAddress4());
        return p04;
    }
    
    
}
