package task2.person;

import task2.address.Address;

import java.time.LocalDate;

/**
 * TABLE PERSON (
 *         first_name varchar(20),
 *          last_name varchar(20),
 *          DoB date,
 *          sex char(1),
 *          home_address int,
 *          work_Address int
 *         )
*/
public class Person {
    private String firstName;
    private String lastName;
    private LocalDate dob;
    private Sex sex;
    private Address homeAddress;
    private Address workAddress;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Address getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(Address homeAddress) {
        this.homeAddress = homeAddress;
    }

    public Address getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(Address workAddress) {
        this.workAddress = workAddress;
    }

    @Override
    public String toString() {
        return "Person{" + "\n"+
                "firstName='" + firstName + '\'' + "\n"+
                ", lastName='" + lastName + '\'' +"\n"+
                ", dob=" + dob + "\n"+
                ", sex=" + sex + "\n"+
                ", homeAddress=" + homeAddress + "\n"+
                ", workAddress=" + workAddress +
                '}';
    }
}
