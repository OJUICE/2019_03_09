package task2.entry;

import task2.person.Person;

import java.time.LocalDate;

public class Entry {

    private int id;
    private Person person;
    private LocalDate created;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "id=" + id +
                ", person=" + person +
                ", created=" + created +
                '}';
    }
}
