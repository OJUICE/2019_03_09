package task2.entry;

import task2.person.PersonFactory;

import java.time.LocalDate;

public class EntryFactory {

public PersonFactory personFactory = new PersonFactory();
    
    public Entry createEntry01() {
        Entry e01 = new Entry();
        e01.setId(1);
        e01.setPerson(personFactory.createPerson01());
        e01.setCreated(LocalDate.now());
        return e01;
    }

    public Entry createEntry02() {
        Entry e02 = new Entry();
        e02.setId(2);
        e02.setPerson(personFactory.createPerson01());
        e02.setCreated(LocalDate.now());
        return e02;
    }

    public Entry createEntry03() {
        Entry e03 = new Entry();
        e03.setId(3);
        e03.setPerson(personFactory.createPerson01());
        e03.setCreated(LocalDate.now());
        return e03;
    }

    public Entry createEntry04() {
        Entry e04 = new Entry(); //the same as Insert into table (creates a new object/row)
        e04.setId(4);
        e04.setPerson(personFactory.createPerson04());
        return e04;
    }

    public Entry createEntry05() {
        return new Entry();
    }
}
