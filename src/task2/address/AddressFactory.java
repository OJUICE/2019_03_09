package task2.address;

public class AddressFactory {

    public Address createWorkAddress4() {
        Address a04 = new Address(); //type of object //name of object
        a04.setCountry("Bikini Bottom");
        a04.setZip("55555");
        a04.setStreetNumber(5);
        a04.setStreet("fat crab road");
        a04.setCity("Bikini City");
        return a04;
    }

    public Address createWorkAddress() {
        Address a02 = new Address();
        a02.setCity("Miesau");
        a02.setStreet("Wiesenstrasse");
        a02.setStreetNumber(5);
        a02.setZip("66892");
        a02.setCountry("Pineapple Republic");
        return a02;
    }

    public Address createWorkAddress02() {
        Address a04 = new Address();
        a04.setCity("Mannheim");
        a04.setStreet("Weihergasse");
        a04.setStreetNumber(55);
        a04.setZip("66898");
        a04.setCountry("Germany");
        return a04;
    }

    public Address createWorkAddress3() {
        Address a05 = new Address();
        a05.setCity("Brooklyn");
        a05.setStreet("Jewel st");
        a05.setStreetNumber(123);
        a05.setZip("11122");
        a05.setCountry("US Republic");
        return a05;
    }
    
    public Address createHomeAddress() {
        Address a01 = new Address();
        a01.setCity("Eichenau");
        a01.setCountry("Banana Republic");
        a01.setStreet("Bahnhofstrasse");
        a01.setStreetNumber(5);
        a01.setZip("08661");
        a01.setTelephone("01798763492");
        return a01;
    }

    public Address createHomeAddress2() {  //I just added a "2" because it was ambiguous
        Address a03 = new Address();
        a03.setCity("Bikini Bottom");
        a03.setCountry("Great Ocean");
        a03.setStreet("Star Blvd");
        a03.setStreetNumber(10);
        a03.setZip("11101");
        a03.setTelephone("9175303950");
        return a03;
    }

    public Address createHomeAddress3() {  //I just added a "2" because it was ambiguous
        Address a06 = new Address();
        a06.setCity("Atlantis");
        a06.setCountry("Great Ocean");
        a06.setStreet("Starfish Blvd");
        a06.setStreetNumber(1);
        a06.setZip("11111");
        a06.setTelephone("917530456");
        return a06;
    }

    public Address createHomeAddress4() {  //I just added a "2" because it was ambiguous
        Address a07 = new Address();
        a07.setCity("Crater");
        a07.setCountry("Moon Planet");
        a07.setStreet("Great Reef dr");
        a07.setStreetNumber(22);
        a07.setZip("56987");
        a07.setTelephone("666303950");
        return a07;
    }

}
