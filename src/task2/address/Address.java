package task2.address;

public class Address {
    private String street;
    private int streetNumber;
    private String city;
    private String zip;
    private String country;
    private String telephone;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public String toString() {
        return "Address{" + "\n"+
                "street='" + street + '\'' + "\n"+
                ", streetNumber=" + streetNumber + "\n"+
                ", city='" + city + '\'' + "\n"+
                ", zip='" + zip + '\'' + "\n"+
                ", telephone='" + telephone + '\'' + "\n"+
                ", country='" + country + '\'' +
                '}';
    }

    public String getTelephone() {
        return telephone;
    }
}
