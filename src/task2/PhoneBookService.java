package task2;

import task2.address.Address;
import task2.entry.Entry;
import task2.entry.EntryFactory;
import task2.person.Person;
import task2.person.Sex;

import java.time.LocalDate;
import java.time.Month;
import java.util.LinkedList;
import java.util.List;

public class PhoneBookService {

    private EntryFactory entryFactory = new EntryFactory();

    /**
     * This is an official JavaDoc
     * Each method signature consists of four Elements:
     * 1. Access modifier (public/private)
     * 2. Return type or void (void = no type)
     * 3. Name
     * 4. Argument (e.g. String firstname)
     */
    public List<Entry> createAphoneBook(){
        Entry e01 = entryFactory.createEntry01();
        Entry e02 = entryFactory.createEntry02();
        Entry e03 = entryFactory.createEntry03();
        Entry e04 = entryFactory.createEntry04();
        Entry e05 = entryFactory.createEntry05();
        List<Entry> l01 = new LinkedList<>();  //<generic type of list>
        l01.add(e01);
        l01.add(e02);
        l01.add(e03);
        l01.add(e04);
        return l01;
    }





}
